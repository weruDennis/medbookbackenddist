<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_gender extends Model
{    
    public $table = 'tbl_gender';
    protected $fillable = [
        'gender','patient_id'
        ];
}
