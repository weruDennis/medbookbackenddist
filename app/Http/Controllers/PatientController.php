<?php

namespace App\Http\Controllers;
use App\tbl_patient;
use App\tbl_gender;
use App\tbl_service;

use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postPatient( Request $request )
    {
        $response = $request->all();
        $response = $response['data'];

        if ($request) {
            $patient = tbl_patient::create([
                'name' => $response['name'],
                'date_of_birth' => $response['dob']
            ]);
            $patient_id = $patient['id'];

            $gender = tbl_gender::create([
                'gender' => $response['gender'],
                'patient_id' => $patient_id
            ]);

            $service = tbl_service::create([
                'patient_id' => $patient_id,
                'comments' => $response['comments'],
                'service' => $response['service']
            ]);

            if ($patient && $gender && $service) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 500;
        }
    }

    public function getPatients( Request $request )
    {
        $patients = tbl_patient::orderBy('created_at', 'ASC')->get();
        // $patients = $this->fixPatientData($patients);
        return response()->json($patients);
    }

    public function fixPatientData( $data ) { //add gender and service to individual patient
        for ($i=0; $i < count($data); $i++) {
            $patient_id = $data[$i]['patient_id'];            
            $service = tbl_service::where('patient_id','=', $patient_id)->get();
            if (count($service) > 0) {
                array_push($patient, $service[0]['service']);
            }
        }
    }
}
