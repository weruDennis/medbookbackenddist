<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_patient extends Model
{
    public $table = 'tbl_patient';
    protected $fillable = [
        'name', 'patient_id', 'date_of_birth'
    ];
}
