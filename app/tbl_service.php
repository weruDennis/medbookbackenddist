<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_service extends Model
{
    public $table = 'tbl_service';
    protected $fillable = [
        'patient_id', 'comments', 'service'
    ];
}
